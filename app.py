from flask import Flask, render_template, request, redirect, session, send_from_directory
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
from werkzeug.utils import secure_filename
from transliterate import translit
import hashlib
import pymysql
import os
pymysql.install_as_MySQLdb()


# настройки БД
DB = 'mysql'
HOST = 'localhost'
USER = 'root'
PASSWORD = 'root'
DBNAME = 'flask-test-site'


# настройки для загрузчика
UPLOAD_FOLDER = 'uploads'
EXTENSIONS = {
    'DOCS': ['txt', 'pdf', 'doc', 'docx', 'xls', 'xlsx'],
    'PHOTO': ['jpg', 'jpeg', 'gif', 'png'],
    'VIDEO': ['mp4', 'mkv', 'avi'],
    'MUSIC': ['mp3', 'wav'],
    'WEB': ['html', 'css', 'js', 'php', 'py'],
}
DENY_EXTENSIONS = [
    'WEB',
]
ALLOWED_EXTENSIONS = []
for extensions in EXTENSIONS.keys():
    if extensions not in DENY_EXTENSIONS:
        ALLOWED_EXTENSIONS += EXTENSIONS[extensions]


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = DB+'://'+USER+':'+PASSWORD+'@'+HOST+'/'+DBNAME
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.secret_key = 'super secret key'
db = SQLAlchemy(app)


class File(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    path = db.Column(db.String(255), nullable=False)
    public = db.Column(db.Integer, nullable=False, default=0)
    ownerID = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    create_date = db.Column(db.DateTime, default=datetime.utcnow)
    update_date = db.Column(db.DateTime, default=datetime.utcnow)
    user = db.relationship('User', backref=db.backref('files', lazy=True))

    def __init__(self, name, path, public, owner):
        self.name = name.strip()
        self.path = path.strip()
        self.public = public
        self.ownerID = owner


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(255), nullable=False)
    password = db.Column(db.String(255), nullable=False)
    name = db.Column(db.String(255), nullable=False)
    levelID = db.Column(db.Integer, db.ForeignKey('user_level_dictionary.id'), nullable=False)
    file = db.relationship('File', backref=db.backref('users', lazy=True))
    userLevel = db.relationship('UserLevel', backref=db.backref('users', lazy=True))

    def __init__(self, login, password, name, levelID):
        self.login = login.strip()
        self.password = password
        self.name = name.strip()
        self.levelID = levelID


class UserLevel(db.Model):
    __tablename__ = 'user_level_dictionary'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    privileges = db.Column(db.Integer, nullable=False)
    user = db.relationship('User', backref=db.backref('level', lazy=True))


def get_files(limit = None):
    owner = session['test-site']['userID'] if 'test-site' in session else None
    if owner: files = File.query.filter((File.ownerID==owner)|(File.public==1))
    else: files = File.query.filter(File.public==1)
    if limit: files = files.limit(limit)
    files = files.all()
    return {'count': len(files), 'data': files}


@app.route('/')
@app.route('/home')
def index():
    return render_template('index.html', files=get_files(5))


@app.route('/files')
def file():
    return render_template('file.html', files=get_files())


@app.route('/resume')
def resume():
    return render_template('resume.html')


@app.route('/register', methods=['GET', 'POST'])
def register():
    error_message = ''
    URL = request.args.get('URL') if request.args.get('URL') else '/'
    if 'test-site' in session: return redirect(URL)
    elif request.method == 'POST':
        if request.form['login'] and request.form['password'] and request.form['password_repeat'] and request.form['name']:
            login = request.form['login']
            password = hashlib.md5(request.form['password'].encode()).hexdigest()
            password2 = hashlib.md5(request.form['password_repeat'].encode()).hexdigest()
            name = request.form['name']
            user = User.query.filter_by(login=login).first()
            if user:
                error_message = 'Пользователь с данным логином уже существует'
            elif password == password2:
                try:
                    user = User(login, password, name, 1)
                    db.session.add(user)
                    db.session.commit()
                    return redirect(URL)
                except:
                    return render_template('register.html', error_message='При регистрации произошла ошибка. Попробуйте позже')
            else: error_message = 'Пароли не совпадают'
        else: error_message = 'Необходимо заполнить все поля'
    return render_template('register.html', error_message=error_message)


@app.route('/login', methods=['GET', 'POST'])
def login():
    error_message = ''
    URL = request.args.get('URL') if request.args.get('URL') else '/'
    if 'test-site' in session: return redirect(URL)
    elif request.method == 'POST':
        if request.form['login'] and request.form['password']:
            login = request.form['login']
            password = hashlib.md5(request.form['password'].encode()).hexdigest()
            user = User.query.filter(User.login==login, User.password==password).first()
            if user:
                session['test-site'] = {
                    'userID': user.id,
                    'userLogin': user.login,
                    'userName': user.name,
                    'userLevelPrivileges': user.level.privileges,
                    'userLevelName': user.level.name,
                }
                return redirect(URL)
            else: error_message = 'Ошибка ввода данных. Проверьте логин и пароль'
        else: error_message = 'Необходимо заполнить все поля'
    return render_template('login.html', error_message=error_message)


@app.route('/logout', methods=['GET'])
def logout():
    URL = request.args.get('URL') if request.args.get('URL') else '/'
    if 'test-site' in session: session.pop('test-site')
    return redirect(URL)


@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    error_message = 'При добавлении файла произошла ошибка. Попробуйте позже или обратитесь к администратору'
    login = False
    haveFile = addedFile = []
    try:
        if 'test-site' in session:
            URL = request.args.get('URL') if request.args.get('URL') else '/'
            if request.method == 'POST':
                names = request.form['name'].split(', ')
                files = request.files.getlist('file')
                # public = request.form['public']
                public = 0
                if files:
                    for i in range(len(files)):
                        haveFile.append(False)
                        addedFile.append(False)
                        name = names[i]
                        file = files[i]
                        if not name: name = file.filename
                        if '.' in name and name.rsplit('.', 1)[1] not in ALLOWED_EXTENSIONS:
                            return render_template('error.html', error_message='Расширение ' + name.rsplit('.', 1)[1] + ' запрещено. Обратитесь к администратору')
                        name = secure_filename(translit(name, 'ru', reversed=True))
                        path = app.config['UPLOAD_FOLDER'] + '/' + session['test-site']['userLogin']
                        os.makedirs(path, exist_ok=True)
                        for item in os.listdir(path=path):
                            if item == name: haveFile[i] = True
                        if file.save(path + '/' + name):
                            addedFile[i] = True
                        if haveFile[i]:
                            file = File.query.filter(File.name==name, File.path==path).first()
                            file.update_date = datetime.utcnow()
                        else: file = File(name, path, public, session['test-site']['userID'])
                        db.session.add(file)
                    db.session.commit()
                    return redirect(URL)
        else:
            error_message = 'Для того, чтобы загрузить файл: '
            login = True
        return render_template('error.html', error_message=error_message, login=login)
    except:
        for addFile in addedFile:
            if addFile: os.remove(path + '/' + name)
        return render_template('error.html', error_message=error_message, login=login)


@app.route('/download/<name>-<id>', methods=['GET'])
def download_file(name, id):
    error_message = 'Не удалось скачать файл. Попробуйте позже или обратитесь к администратору'
    file = File.query.get(id)
    if file:
        if file.public == 1 or file.ownerID == session['test-site']['userID']:
            return send_from_directory(directory=file.path, filename=file.name)
        else: error_message = 'Для того, чтобы скачать данный файл, необходимо быть его владельцем'
    return render_template('error.html', error_message=error_message)


@app.route('/update',  methods=['GET', 'POST'])
def update_file():
    error_message = 'При изменении файла произошла ошибка. Попробуйте позже или обратитесь к администратору'
    login = False
    try:
        if 'test-site' in session:
            URL = request.args.get('URL') if request.args.get('URL') else '/'
            if request.method == 'POST':
                id = request.form['id']
                name = request.form['name']
                # public = request.form['public']
                public = 1
                file = File.query.get(id)
                if file:
                    if file.ownerID == session['test-site']['userID']:
                        if '.' in name and name.rsplit('.', 1)[1] not in ALLOWED_EXTENSIONS:
                            return render_template('error.html', error_message='Расширение ' + name.rsplit('.', 1)[1] + ' запрещено. Обратитесь к администратору')
                        os.rename(file.path + '/' + file.name, file.path + '/' + name)
                        file.name = name
                        file.public = public
                        file.update_date = datetime.utcnow()
                        db.session.add(file)
                        db.session.commit()
                        return redirect(URL)
                    else: error_message = 'Для того, чтобы изменить файл, необходимо быть его владельцем'
        else:
            error_message = 'Для того, чтобы изменить файл, необходимо быть его владельцем: '
            login = True
        return render_template('error.html', error_message=error_message, login=login)
    except:
        return render_template('error.html', error_message=error_message, login=login)


@app.route('/delete/<name>-<id>', methods=['GET'])
def delete_file(name, id):
    error_message = 'При удалении файла произошла ошибка. Попробуйте позже или обратитесь к администратору'
    login = False
    try:
        if 'test-site' in session:
            URL = request.args.get('URL') if request.args.get('URL') else '/'
            file = File.query.get(id)
            if file:
                if file.ownerID == session['test-site']['userID']:
                    os.remove(file.path + '/' + file.name)
                    if not len(os.listdir(path=file.path)):
                        os.rmdir(file.path)
                    db.session.delete(file)
                    db.session.commit()
                    return redirect(URL)
                else: error_message = 'Для того, чтобы удалить файл, необходимо быть его владельцем'
        else:
            error_message = 'Для того, чтобы удалить файл, необходимо быть его владельцем: '
            login = True
        return render_template('error.html', error_message=error_message, login=login)
    except:
        return render_template('error.html', error_message=error_message, login=login)


if __name__ == '__main__':
    app.run(debug = True)