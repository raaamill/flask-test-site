-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Апр 24 2021 г., 11:58
-- Версия сервера: 8.0.19
-- Версия PHP: 7.4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `rosneft-disk`
--

-- --------------------------------------------------------

--
-- Структура таблицы `file`
--

CREATE TABLE `file` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `public` int NOT NULL,
  `ownerID` int NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `file`
--

INSERT INTO `file` (`id`, `name`, `path`, `public`, `ownerID`, `create_date`, `update_date`) VALUES
(1, 'my_photo.jpg', 'uploads/root', 1, 1, '2021-04-24 08:57:02', '2021-04-24 08:57:02'),
(2, 'resume.pdf', 'uploads/root', 1, 1, '2021-04-24 08:57:41', '2021-04-24 08:57:41');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `levelID` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `login`, `password`, `name`, `levelID`) VALUES
(1, 'root', '63a9f0ea7bb98050796b649e85481845', 'Суперадмин', 1),
(2, 'admin1', 'e00cf25ad42683b3df678c61f42c6bda', 'Админ1', 2),
(3, 'admin2', 'c84258e9c39059a89ab77d846ddab909', 'Админ2', 2),
(4, 'admin3', '32cacb2f994f6b42183a1300d9a3e8d6', 'Админ3', 2),
(5, 'moderator1', '38caf4a470117125b995f7ce53e6e6b9', 'Модератор1', 3),
(6, 'moderator2', '95d88ad73653fc7ad4fec3bc56677c3c', 'Модератор2', 3),
(7, 'moderator3', '076548525cf0031fb50a6e948ecf4607', 'Модератор3', 3),
(8, 'monitoring', '89948c7f4890af5ff18524b4fc3f3611', 'Мониторинг', 4),
(9, 'test-site', 'e1f1ff5e93e56d27acf5ebbdde864c0d', 'Test Site', 5);

-- --------------------------------------------------------

--
-- Структура таблицы `user_level_dictionary`
--

CREATE TABLE `user_level_dictionary` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `privileges` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_level_dictionary`
--

INSERT INTO `user_level_dictionary` (`id`, `name`, `privileges`) VALUES
(1, 'Суперадминистратор', 10),
(2, 'Администратор', 8),
(3, 'Модератор', 5),
(4, 'Мониторинг', 3),
(5, 'Пользователь', 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_level_dictionary`
--
ALTER TABLE `user_level_dictionary`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `file`
--
ALTER TABLE `file`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT для таблицы `user_level_dictionary`
--
ALTER TABLE `user_level_dictionary`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
