$('.nav__logout').on('mouseenter', function() {
    $('.user__name').animate({ 'right': '0' }, 200);
});

$('.nav__logout').on('mouseleave', function() {
    $('.user__name').animate({ 'right': '-15rem' }, 200);
});

$('.go__back').on('click', function() {
    history.back();
})

//$('#publicCheckCreate, #publicCheckUpdate').on('click', function() {
//    if ($(this).attr('checked') == 'checked') $(this).removeAttr('checked');
//    else $(this).attr('checked', true);
//})

$('.createButton').on('click', function() {
    var name = $('#createFile').find('.fileName'),
        label = $('#createFile').find('.custom-file-label');
    name.val('');
    label.html('Файл');
//    $('#publicCheckCreate').removeAttr('checked');
})

$('table').on('click', '.updateButton', function() {
    var file_id = $(this).parent().parent().find('.fileIDTable').attr('data'),
        file_name = $(this).parent().parent().find('.fileNameTable a').html(),
        id = $('#updateFile').find('.fileID'),
        name = $('#updateFile').find('.fileName'),
        public = Number($(this).parent().parent().find('.filePublicTable').attr('data'));
    id.val(file_id)
    name.val(file_name);
//    if (public == 1) $('#publicCheckUpdate').attr('checked', true);
//    else $('#publicCheckUpdate').removeAttr('checked');
})

$('#createFile, #updateFile').on('change', '.inputFile', function() {
    console.log(this.files);
    var files = this.files,
        name = $(this).parent().parent().find('.fileName'),
        label = $(this).parent().find('.custom-file-label');
    files_name = files[0].name;
    for (var i = 1; i < files.length; i++) {
        files_name += ', ' + files[i].name
    }
    name.val(files_name);
    label.html(files_name);
});